var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../server'); //lanzar el server

describe('First test suite',
  function() {
    it('Test that DuckDuckGo works',
      function(done) {
          chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(
              function(err,res) {
                console.log("Request has ended");
                console.log(err);
                //console.log(res);
                res.should.have.status(200);
                done();
              }
          )
      }
    )
  }
);

describe('Test de api de usuarios tech u',
  function() {
    it('Prueba de que la api de usuarios esta funcionado',
      function(done) {
          chai.request('http://localhost:3000')
          .get('/apitechu/v1')
          .end(
              function(err,res) {
                console.log("Request has ended");
                console.log(err);
                //console.log(res);
                res.should.have.status(200);
                res.body.msg.should.be.eql("Hola desde APITechU");
                done();
              }
          )
      }
    ),
    it('Prueba de que la api devuelve una lista de usuarios correctos',
      function(done) {
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
              function(err,res) {
                console.log("Request has ended");
                console.log(err);
                //console.log(res);
                res.should.have.status(200);
                res.body.should.be.a("array");

                for (user of res.body) {
                  user.should.have.property('email');
                  user.should.have.property('password');
                }
                done();

              }
          )
      }
    )
  }
)
