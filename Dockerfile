FROM node

WORKDIR /apitechu

ADD . /apitechu

# Abrimos el puerto donde escucha la aplicación (3000)
EXPOSE 3000

CMD ["npm","start"]
