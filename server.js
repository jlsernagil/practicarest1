// GLOBAL VARIABLES
var express = require("express");
var app = express();
var port = process.env.port || 3000;
var bodyParser= require('body-parser');
var users_file = './usuarios.json';

//definimos variables para el consumo de la ApI de Mlab, para utilizarlo posteriormente
var baseMLabURL = "https://api.mlab.com/api/1/databases/techu/collections/";
var mLabAPIKey = "apiKey=hxRH6-EyuFIzGIbYrfMUBhgX7beIHfth";
var requestJson = require('request-json');

app.use(bodyParser.json() )
app.listen(port);

console.log("Escuchando en el puerto: " + port);

//test connection
app.get("/apitechu/v1",
  function (req,res) {
    console.log("GET /apitechu/v1");
    res.send({"msg" : "Hola desde APITechU"});
  }
);

//listar usuarios actuales
app.get("/apitechu/v1/users",
  function (req,res) {
    console.log("GET /apitechu/v1/users");
    //res.sendFiles("./usuarios.json"); DEPRECATED

    //res.sendFile('usuarios.json', {root: __dirname});

    var users = require(users_file);
    res.send(users);
  }
);

//listar usuarios actuales
app.get("/apitechu/v1/users/:id",
  function (req,res) {
    var user_id = req.params.id;
    console.log("GET /apitechu/v1/users/" + user_id);
    var users = require(users_file);
    //users.splice();
    for (user in users) {
      //console.log("cliente localizado " + users[user] + " con id " + users[user].id + " contra id " + user_id);
      if (users[user].id == user_id) {
        console.log("cliente localizado " + users[user] + " con id " + users[user].id);
        var found = true;
        var json_user = JSON.stringify(users[user]);
        res.send(json_user);
      }
    }
    if (!found) {
        res.send("No user found");
    }

  }
);

// añadir usuario
app.post("/apitechu/v1/users",
  function (req,res) {
    console.log("POST /apitechu/v1/users");
    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    }

    var users = require(users_file);
    users.push(newUser);
    /*
    var fs = require('fs');
    var jsonUserData = JSON.stringify(users);
    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
      function (err) {
        if (err) {
          var msg = "Error al escribir el fichero usuarios";
          console.log(msg);
        } else {
          var msg = "Usuario persistido"
          console.log(msg);
        }
        res.send({"msg": msg});
      }
    )
    */
    writeUserDataToFile(users, users_file);
    res.send({"msg": "ok"});
  }
);

// añadir usuario
app.post("/apitechu/v1.2/users",
  function (req,res, other) {
    console.log("POST /apitechu/v2/users");
    console.log("Other: " + other);

    var users = require(users_file);

/*
    var newUser = {
      "id" : req.body.first_name;
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    }
    users.push(newUser);
*/
    users.push(req.body);
    writeUserDataToFile(users, users_file);
    res.send({"msg": "ok"});
  }
);

app.delete("/apitechu/v1/users/:id",
  function (req,res) {
    user_id = req.params.id;
    console.log("DELETE /apitechu/v1/users/" + user_id);
    var users = require(users_file);
    var new_users = [];
    //users.splice();
    for (user in users) {
      //console.log("cliente localizado " + users[user] + " con id " + users[user].id + " contra id " + user_id);
      if (users[user].id != user_id) {
        new_users.push(users[user]);
      } else {
        var found = true;
        console.log("cliente localizado " + users[user] + " con id " + users[user].id);
      }
    }
    if (!found) {
        res.send("No user found");
    } else {
      writeUserDataToFile(new_users, users_file);
      res.send("Cliente eliminado");
    }
  }
);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req,res) {
    console.log("/apitechu/v1/monstruo/");

    console.log("headeres");
    console.log(req.headers);

    console.log("query string");
    console.log(req.query);

    console.log("parametros");
    console.log(req.params);

    console.log("body");
    console.log(req.body);
  }
);

//test connection
app.post("/apitechu/v1/login",
  function (req,res) {
    console.log("POST /apitechu/v1/login");
    var email_in = req.body.email;
    var password_in = req.body.password;

    var users = require(users_file);

    var found_user = null;

    for (user of users) {
      if (user.email == email_in) {
        console.log("cliente localizado ");
        found_user = user;
        break;
      }
    }

    if (found_user == null) {
        res.send("Wrong password or user (user)");
    } else if (found_user.password == password_in) {
        found_user.logged = true;
        writeUserDataToFile(users ,users_file)
        res.send("Login correcto \n"+
                 "Welcome: Mr." + found_user.last_name + "\n" + " your id is: " + found_user.id);
    } else {
      res.send("Wrong password or user (pass)");
    }

  }
);

app.post("/apitechu/v1/logout",
  function (req,res) {
    console.log("POST /apitechu/v1/login");
    var id_in = req.body.id;

    var users = require(users_file);

    var found_user = null;
    for (user of users) {
      if (user.id == id_in) {
        console.log("cliente localizado ");
        found_user = user;
        break;
      }
    }

    if (found_user == null) {
        res.send("User not found");
    } else if (found_user.logged != true) {
      res.send("logout error \nUser not logged");
    } else {
      delete found_user.logged;
      writeUserDataToFile(users ,users_file)
      res.send("Logout correct \nBye Bye: Mr." + found_user.last_name);
    }
  }
);

function writeUserDataToFile(usersData,file) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(usersData);

  fs.writeFile (file, jsonUserData, "utf8",
    function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Data stored");
      }
    }
  )
};

//
// creamos una nueva función para obtener los datos desde mLab
app.get("/apitechu/v2/users",
  function(req,res) {
    console.log("GET /apitechu/v2/users");

//creamos el cliente
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("cliente http creado");

//hacemos las peticiones a partir de la base, y añadiendo los filtros y el apiKey
    httpClient.get("users?"+ mLabAPIKey,
       function (err, resMLab, body) {
         var response = !err ? body : {
           "msg" : "Error obteniendo usuarios"
         }
         res.send (response);
       }
     )
  }
);


//
// creamos una nueva función para obtener los datos desde mLab
app.get("/apitechu/v2/users/:id",
  function(req,res) {
    console.log("GET /apitechu/v2/users/:id");

//creamos el cliente
    var httpClient = requestJson.createClient(baseMLabURL);

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';
//hacemos las peticiones a partir de la base, y añadiendo los filtros y el apiKey
    httpClient.get("users?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {/*
         var response = !err ? body : {
           "msg" : "Error obteniendo usuarios"
         }
         res.send (response);
*/
          var response = null;
          if (err) {
            response = {"msg" : "Error obteniendo usuario."};
            res.status(500);
          } else if (body.length > 0) {
            response = body;
          } else {
            response = {"msg": "Usuario no encontrado"};
            res.status(404);
          }
          res.send (response);
        }
     )
  }
);


//test connection
app.post("/apitechu/v2/login",
  function (req,res) {
    console.log("POST /apitechu/v2/login");
    var email_in = req.body.email;
    var password_in = req.body.password;

    var httpClient = requestJson.createClient(baseMLabURL);

    // variables para mantener datos en el flujo
    var user = null;
    var msg_err = null;
    var status_err = null;

    // Recuperar el usuario
    var query = 'q={"email": "' + email_in + '"}';
    httpClient.get("users?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {
          if (err) {
            msg_err = {"msg" : "Error obteniendo usuario."};
          } else if (body.length != 1) {
            msg_err = {"msg" : "Error localizando usuario."};
          } else {
            console.log("localizado usuario");
            user = body[0];
          };
          console.log("user --> " + user);

          // si hemos localizado un usuario
          if (user == null) {
            res.status(500);
            res.send(msg_err);
          } else {
              if (user["password"] != password_in) {
                msg_err = {"msg" : "Error password incorrecta."};
                res.status(500);
                res.send(msg_err);
              } else if (user["logged"] == true) {
                msg_err = {"msg" : "Usuario logado."};
                res.status(500);
                res.send(msg_err);
              } else {
                // actualizamos el estado del usuarios
                console.log(" actualizamos el estado del usuarios");
                // actualizamos el estado del usuarios
                var updateBody = JSON.parse('{"$set" : {"logged": true}}');
                httpClient.put("users?"+ query + "&" + mLabAPIKey, updateBody,
                   function (err, resMLab, body) {
                      var response = null;
                      if (err) {
                        msg_err = {"msg" : "Error actualizando estado usuario."};
                        res.status(500);
                        res.send(msg_err);
                      } else {
                        res.send(JSON.stringify({"msg":"Usuario logado correcta",
                                                 "id" : user["id"]}));
                        //res.send("Usuario "+ user["first_name"] + " logado correctamente");
                      }
                    }
                 )
              }
          };
        }
     )
  }
);


//test connection
app.post("/apitechu/v2/logout",
  function (req,res) {
    console.log("POST /apitechu/v2/logout");
    var id_in = req.body.id;

    var httpClient = requestJson.createClient(baseMLabURL);

    // variables para mantener datos en el flujo
    var user = null;
    var msg_err = null;

    // Recuperar el usuario
    var query = 'q={"id": ' + id_in + '}';
    httpClient.get("users?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {
          if (err) {
            msg_err = {"msg" : "Error obteniendo usuario."};
          } else if (body.length != 1) {
            msg_err = {"msg" : "Error localizando usuario."};
          } else {
            console.log("localizado usuario");
            user = body[0];
          };
          console.log("user --> " + user);

          // si hemos localizado un usuario
          if (user == null) {
            res.status(500);
            res.send(msg_err);
          } else {
              if (user["logged"] == true) {
                // actualizamos el estado del usuarios
                console.log(" actualizamos el estado del usuario");
                // actualizamos el estado del usuarios
                var updateBody = JSON.parse('{"$unset" : {"logged":""}}');
                httpClient.put("users?"+ query + "&" + mLabAPIKey, updateBody,
                   function (err, resMLab, body) {
                      var response = null;
                      if (err) {
                        msg_err = {"msg" : "Error actualizando estado usuario."};
                        res.status(500);
                        res.send(msg_err);
                      } else {
                        res.send("Adios "+ user["first_name"]);
                      }
                    }
                 )
              } else { // El usuario no está logado
                msg_err = {"msg" : "Usuario no logado."};
                res.status(500);
                res.send(msg_err);
              }
          };
        }
     )
  }
);

//listar usuarios actuales
app.get("/apitechu/v1/users/:id/contracts",
  function (req,res) {
    var user_id = req.params.id;
    console.log("GET /apitechu/v1/users/" + user_id + "/contracts");

    var httpClient = requestJson.createClient(baseMLabURL);

    var id = req.params.id;
    var query = 'q={"client_id":' + id + '}&l=10';
//hacemos las peticiones a partir de la base, y añadiendo los filtros y el apiKey
    httpClient.get("contract?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {
         console.lo
          var response = null;
          if (err) {
            response = {"msg" : "Error obteniendo listado de contratos."};
            res.status(500);
          } else if (body.length > 0) {
            response = body;
          } else {
            response = {"msg": "No se han encontrado contratos"};
            res.status(404);
          }
          res.send (response);
        }
     )

  }
);
